﻿Shader "Anclin/Portal"
{
    Properties
    {
        // we have removed support for texture tiling/offset,
        // so make them not be displayed in material inspector
        _NoiseTex ("Noise", 2D) = "black" {}
        _NoiseScale ("Noise Scale", Vector) = (.01, .01, 1, 1)
        _RotationSpeed ("Rotation Speed", Range(-10, 10)) = 1
        _SpiralScale ("Spiral Scale", Vector) = (1, 1, 5, 0)
        _Color1 ("Color 1", Color) = (0, 0, 0, 1)
        _Color2 ("Color 2", Color) = (1, 1, 1, 1)
    }
    SubShader
    {
        Pass
        {
            CGPROGRAM
            // use "vert" function as the vertex shader
            #pragma vertex vert
            // use "frag" function as the pixel (fragment) shader
            #pragma fragment frag

            // vertex shader inputs
            struct appdata
            {
                float4 vertex : POSITION; // vertex position
                float2 uv : TEXCOORD0; // texture coordinate
            };

            // vertex shader outputs ("vertex to fragment")
            struct v2f
            {
                float2 uv : TEXCOORD0; // texture coordinate
                float4 vertex : SV_POSITION; // clip space position
            };

            // vertex shader
            v2f vert (appdata v)
            {
                v2f o;
                // transform position to clip space
                // (multiply with model*view*projection matrix)
                o.vertex = UnityObjectToClipPos(v.vertex);
                // just pass the texture coordinate
                o.uv = v.uv;
                return o;
            }

            // texture we will sample
            sampler2D _NoiseTex;
            fixed4 _NoiseScale;
            fixed _RotationSpeed;
            fixed4 _SpiralScale;
            fixed4 _Color1;
            fixed4 _Color2;

            // pixel shader; returns low precision ("fixed4" type)
            // color ("SV_Target" semantic)
            fixed4 frag (v2f i) : SV_Target
            {
                // sample texture and return it
                fixed noiseAnim = _Time.x * _NoiseScale.z;
                fixed noiseAnim2 = _Time.x * _NoiseScale.w;
                fixed4 noise_tex = tex2D(_NoiseTex, i.uv * _NoiseScale.x + fixed2(noiseAnim,noiseAnim)) * 2 - 1;
                fixed4 noise_tex2 = tex2D(_NoiseTex, i.uv * _NoiseScale.y + fixed2(noiseAnim2,noiseAnim2)) * 2 - 1;
                // fixed4 noise_tex2 = tex2D(_NoiseTex, i.uv * 5 + -_Time.x * _NoiseScale.zw) * 2 - 1;
                fixed2 uv = i.uv;
                uv -= .5;
                uv *= _SpiralScale.xy;

                fixed noiseAmout = 1;
                uv += (noise_tex + noise_tex2) * _NoiseScale.xy;

                fixed a = atan2(uv.x, uv.y) / 6.28 + .5 + _Time.x * _RotationSpeed;
                float l = length(uv);

                float c = a + l * _SpiralScale.z;
                // c = frac(c);
                c = sin(c*6.28)*.5+.5;

                //fixed4 col = fixed4(i.uv.x, i.uv.y, 0, 1);
                // fixed4 col = fixed4(c,c,c,1);
                fixed4 col = lerp(_Color1, _Color2, c);

                // col = noise;
                return col;
            }
            ENDCG
        }
    }
}
