﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum State {
    Walking,
    Flying,
    NONE
}

public class CharController : MonoBehaviour {

    [Header("REFs")]
    [SerializeField] private Transform SpawnTransform;
    [SerializeField] private Camera Camera = null;
    [SerializeField] private CharacterController CharacterController = null;
    [SerializeField] private Animator Animator = null;
    [SerializeField] private CharController MirrorSource = null;

    [Header("SETTINGS")]
    [SerializeField] private float Gravity = -10;
    [SerializeField] private float WalkSpeed = 1;
    [SerializeField] private float RunSpeed = 3;
    [SerializeField] private float SpeedSmoothTime = .2f;
    [SerializeField] private float TurnSmoothTime = .1f;
    [SerializeField] private float PushPower = 1.0f;
    [SerializeField] private float WalkAnimationTransitionDampTime = .1f;
    [SerializeField] private bool DoMirror;

    [Header("ATTRACTOR")]
    [SerializeField] private List<LineAttractor> LineAttractorsList;
    [SerializeField] private bool UseAttractor = true;
    [SerializeField] float ResetTresholdY = 0;

    [Header("DEBUG")]
    [SerializeField] private float FlySpeed = 1;

    [HideInInspector] public Action OnDeathAction;
    [HideInInspector] public Action<Collider> OnCollectableHit;

#if DEBUG
    private int HistoryCount = 50;
    private List<Vector3> PositionHistory = new List<Vector3>();
    private bool IsFlying = false;
#endif

    private State CurrentState = State.NONE;

    private float CurrentSpeed;
    private float SpeedSmoothVelocity;
    private float VelocityY;

    private float CurrentRotation;
    private float TurnSmoothVelocity;

    private Vector3 InputDir;
    private Vector3 InputDirRelToCamera;

    private bool Crouching;
    private Vector3 CrouchSmoothVeolocity;

    private Vector3 SpawnPosition;
    private Quaternion SpawnRotation;


    void Start() {
        // get all attractors on scene
        LineAttractor[] attractors = FindObjectsOfType<LineAttractor>();
        LineAttractorsList.Clear();
        LineAttractorsList.AddRange(attractors);

        // init position
        SpawnPosition = SpawnTransform.position;
        SpawnRotation = SpawnTransform.rotation;

        EnterState(State.Walking);
    }

    public void Respawn() {
        transform.position = SpawnPosition;
        transform.rotation = SpawnRotation;
        EnterState(State.Walking);
    }

    private void Died() {
        CurrentSpeed = 0;
        VelocityY = 0;
        Respawn();
        if (OnDeathAction != null)
            OnDeathAction();
    }

    void LateUpdate() {
        if (DoMirror) {
            // copy position
            Vector3 mirrorPos = MirrorSource.transform.position;
            mirrorPos.z = -mirrorPos.z;
            transform.position = mirrorPos;

            // copy angle
            Vector3 eulerAngles = MirrorSource.transform.rotation.eulerAngles;
            eulerAngles.y = -eulerAngles.y + 180;
            transform.eulerAngles = eulerAngles;

            // copy animation
            Animator.SetFloat("SpeedPercent", MirrorSource.GetWalkSpeedPercent());
        }
    }

    void Update() {
        if (DoMirror) return;

        switch (CurrentState) {
            case State.Walking:
                ExecuteStateWalking();
                break;
            case State.Flying:
                ExecuteStateFlying();
                break;
            case State.NONE:
                break;
            default:
                break;
        }

#if UNITY_EDITOR
        // store debug data
        PositionHistory.Add(transform.position);
        if (PositionHistory.Count > HistoryCount)
            PositionHistory.RemoveAt(0);
#endif
    }

    private void EnterState(State newState) {
        if (newState == CurrentState) {
            Debug.Log(string.Format("No state change, already in {0} state!", CurrentState));
            return;
        }

        // exit current state
        Debug.Log(string.Format("ExitState {0}", CurrentState));
        switch (CurrentState) {
            case State.Walking:
                ExitStateWalking();
                break;
            case State.Flying:
                ExitStateFlying();
                break;
            case State.NONE:
                break;
            default:
                break;
        }

        // enter new state
        Debug.Log(string.Format("EnterState {0}", newState));
        CurrentState = newState;
        switch (newState) {
            case State.Walking:
                EnterStateWalking();
                break;
            case State.Flying:
                EnterStateFlying();
                break;
            case State.NONE:
                break;
            default:
                break;
        }
    }

    // STATE WALKING

    private void EnterStateWalking() {
        //throw new NotImplementedException();
    }

    private void ExitStateWalking() {
        //throw new NotImplementedException();
    }

    private void ExecuteStateWalking() {
        // input
        InputDir = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
        InputDirRelToCamera = (Utils.RotateY(Camera.transform.eulerAngles.y * Mathf.Deg2Rad) * InputDir).normalized;

        // move dir
        Vector3 moveDir = InputDirRelToCamera;

        // apply attractor force
        if (UseAttractor) {
            float closestDist = Mathf.Infinity;
            Vector3 closestDir = moveDir;
            foreach (var attractor in LineAttractorsList) {
                Vector4 attractorDir = AttractorDir(InputDirRelToCamera, attractor);
                if (attractorDir.w < closestDist) {
                    closestDist = attractorDir.w;
                    closestDir = attractorDir;
                }
            }
            moveDir = closestDir;
        }

        // rotate
        if (InputDir != Vector3.zero) {
            float targetRotation = Mathf.Atan2(moveDir.x, moveDir.z) * Mathf.Rad2Deg;
            CurrentRotation = Mathf.SmoothDampAngle(CurrentRotation, targetRotation, ref TurnSmoothVelocity, TurnSmoothTime);
            transform.eulerAngles = Vector3.up * CurrentRotation;
        }

        // calculate movement
        bool run = Input.GetKey(KeyCode.LeftShift) || Input.GetKey("joystick button 7");
        float targetSpeed = (run ? RunSpeed : WalkSpeed) * InputDir.magnitude;
        //targetSpeed *= run ? RunSpeed : 1;
        CurrentSpeed = Mathf.SmoothDamp(CurrentSpeed, targetSpeed, ref SpeedSmoothVelocity, SpeedSmoothTime);

        //gravity
        VelocityY += Gravity * Time.deltaTime;
        Vector3 velocity = moveDir * CurrentSpeed + Vector3.up * VelocityY;

        // apply movement
        CharacterController.Move(velocity * Time.deltaTime);

        if (CharacterController.isGrounded) {
            VelocityY = 0;
        }

        // update animations
        float animPercent = CharacterController.velocity.magnitude / WalkSpeed;
        animPercent *= CharacterController.isGrounded ? 1 : 0;
        Animator.SetFloat("SpeedPercent", animPercent, WalkAnimationTransitionDampTime, Time.deltaTime);

        // check for death
        if (transform.position.y < ResetTresholdY) {
            Died();
        }

#if UNITY_EDITOR
        if (Input.GetKeyDown(KeyCode.E)
            || Input.GetKeyDown(KeyCode.Space)
            || Input.GetKeyDown("joystick button 4")
            || Input.GetKeyDown("joystick button 3")) {
            EnterState(State.Flying);
        }
#endif
    }

    // STATE FLYING

    private void EnterStateFlying() {
        Animator.SetFloat("SpeedPercent", 0);
        CurrentSpeed = 0;
    }

    private void ExitStateFlying() {
    }

    private void ExecuteStateFlying() {
        InputDir = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
        InputDirRelToCamera = (Utils.RotateY(Camera.transform.eulerAngles.y * Mathf.Deg2Rad) * InputDir).normalized;

        // move dir
        float flyUp = Input.GetKey(KeyCode.E) || Input.GetKey("joystick button 4") ? 1 : 0;
        float flyDown = Input.GetKey(KeyCode.Q) || Input.GetKey("joystick button 6") ? 1 : 0;
        Vector3 moveDir = InputDirRelToCamera;
        moveDir += Vector3.up * flyUp;
        moveDir += Vector3.down * flyDown;
        moveDir *= (Input.GetKey(KeyCode.LeftShift) || Input.GetKey("joystick button 7") ? 3 : 1);

        // rotate
        if (InputDir != Vector3.zero) {
            float targetRotation = Mathf.Atan2(moveDir.x, moveDir.z) * Mathf.Rad2Deg;
            CurrentRotation = Mathf.SmoothDampAngle(CurrentRotation, targetRotation, ref TurnSmoothVelocity, TurnSmoothTime);
            transform.eulerAngles = Vector3.up * CurrentRotation;
        }

        float targetSpeed = FlySpeed * Mathf.Max(InputDir.magnitude, Mathf.Max(flyUp, flyDown));
        CurrentSpeed = Mathf.SmoothDamp(CurrentSpeed, targetSpeed, ref SpeedSmoothVelocity, SpeedSmoothTime);
        CharacterController.Move(moveDir * CurrentSpeed * Time.deltaTime * 60);

        // back to walking state
        if (Input.GetKeyDown(KeyCode.Space)
            || Input.GetKeyDown("joystick button 3"))
            EnterState(State.Walking);
    }

    // this script pushes all rigidbodies that the character touches
    void OnControllerColliderHit(ControllerColliderHit hit) {
        Rigidbody body = hit.collider.attachedRigidbody;

        // no rigidbody
        if (body == null || body.isKinematic) {
            return;
        }

        // We dont want to push objects below us
        if (hit.moveDirection.y < -0.3) {
            return;
        }

        // Calculate push direction from move direction,
        // we only push objects to the sides never up and down
        Vector3 pushDir = new Vector3(hit.moveDirection.x, 0, hit.moveDirection.z);

        // If you know how fast your character is trying to move,
        // then you can also multiply the push velocity by that.

        // Apply the push
        body.velocity = pushDir * PushPower;
    }

    private void OnTriggerEnter(Collider other) {
        if (other.gameObject.layer == LayerMask.NameToLayer("Collectable")) {
            if (OnCollectableHit != null)
                OnCollectableHit(other);
        }
    }

    private Vector4 AttractorDir(Vector3 dir, LineAttractor attractor) {
        LineAttractorData data = attractor.Data;
        Vector3 p0 = attractor.P0.position;
        Vector3 p1 = attractor.P1.position;
        Vector3 cp = Utils.ClosestPointOnLine(transform.position, p0, p1);
        Vector3 lineDir = (p1 - p0).normalized;
        int lineAlignment = Vector3.Dot(lineDir, dir) > 0 ? 1 : -1;
        if (dir == Vector3.zero)
            lineAlignment = 0;
        Vector3 attractorDir = lineDir * lineAlignment;
        float d = (transform.position - cp).magnitude;
        float d01 = Mathf.Clamp01(Mathf.InverseLerp(data.Range, 0, d)); // closer to the line, higher the value
        if (data.Curve != null)
            d01 = data.Curve.Evaluate(d01);
        Vector3 lerpDir = Vector3.Lerp(dir, attractorDir, d01 * data.Power);
        return new Vector4(lerpDir.x, lerpDir.y, lerpDir.z, d);
    }

    private float GetWalkSpeedPercent() {
        return Animator.GetFloat("SpeedPercent");
    }

#if DEBUG
    private void OnDrawGizmos() {

        if (DoMirror) return;


        // positions history
        for (int i = 0; i < PositionHistory.Count; i++) {
            Color color = Color.white;
            color.a = Mathf.Lerp(0.1f, 1, (float)i / (float)PositionHistory.Count);
            Gizmos.color = color;
            Gizmos.DrawSphere(PositionHistory[i], .05f);
        }

        // line attractor gizmos
        if (UseAttractor) {

            // get closest attractor
            LineAttractor closestAttractor = null;
            float closestDist = Mathf.Infinity;
            foreach (LineAttractor attractor in LineAttractorsList) {
                Vector4 attractorDir = AttractorDir(InputDirRelToCamera, attractor);
                if (attractorDir.w < closestDist) {
                    closestDist = attractorDir.w;
                    closestAttractor = attractor;
                }

                // gizmos attractor
                Vector3 p0 = attractor.P0.position;
                Vector3 p1 = attractor.P1.position;
                Gizmos.color = Color.red;
                Gizmos.DrawSphere(p0, .2f);
                Gizmos.DrawSphere(p1, .2f);
                Gizmos.DrawLine(p0, p1);
                Utils.GizmosDrawLineRange(p0, p1, attractor.Data.Range);
            }

            // draw closest attractor dirs
            if (closestAttractor != null) {
                LineAttractorData data = closestAttractor.Data;
                Vector3 p0 = closestAttractor.P0.position;
                Vector3 p1 = closestAttractor.P1.position;

                // closest point and line
                Gizmos.color = Color.green;
                Vector3 cp = Utils.ClosestPointOnLine(transform.position, p0, p1);
                Gizmos.DrawSphere(cp, .2f);
                Gizmos.DrawLine(transform.position, cp);

                // temp values
                Vector3 inputDir = InputDirRelToCamera;
                Vector3 lineDir = (p1 - p0).normalized;
                Vector3 attractedDir = (lineDir + inputDir).normalized;
                Vector3 lerpDir = AttractorDir(inputDir, closestAttractor);

                // gizmos dirs
                float rayLen = 2;
                Gizmos.color = Color.white;
                Gizmos.DrawRay(transform.position, inputDir * rayLen * 1.2f);
                Gizmos.color = Color.green;
                Gizmos.DrawRay(transform.position, InputDirRelToCamera * rayLen);
                Gizmos.color = Color.red;
                Gizmos.DrawRay(transform.position - lineDir * rayLen, lineDir * rayLen * 2);
                Gizmos.color = Color.blue * .75f;
                Gizmos.DrawRay(transform.position, attractedDir * rayLen * .9f);
                Gizmos.color = Color.blue;
                Gizmos.DrawRay(transform.position, lerpDir * rayLen * .9f);
            }
        }
    }
#endif
}
