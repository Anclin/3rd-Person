﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public enum GnomeState {
    Idle,
    Broken,
    Repearing,
    Follow,
    RunAway,
    NONE,
}

public class GnomeController : MonoBehaviour {

    [Header("REFERENCES")]
    public GameObject InstancesHolder;
    public GameObject GnomeModel;
    public GameObject GnomeParts;
    public Vector3 Center;

    [Header("PARTICLES")]
    public float PartsSpawnRadius = 0.5f;
    public float ExplosionVelocity = 1;

    [Header("MOVE SETTINGS")]
    [Range(-1, 1)] public float WatchingThreshold = 0.3f;
    public float WalkSpeed = 1;
    public float RunSpeed = 2;
    public float TurnSmoothTime = .1f;
    [Range(0, 1)] public float FollowRatio = .5f;
    public float AvoidRange = 3;
    public float MinFollowRange = 1;
    public float FollowFade = 1;
    public float JumpHeight = 0.5f;
    public float JumpSpeed = 1;
    public AnimationCurve SpeedCurve;
    public AnimationCurve MockingCurve;

    [Header("DISTANCES")]
    public float StartRunningDist = 5;
    public float StopRunningDist = 12;
    public float StartFollowingDist = 10;

    [Header("TIMERS")]
    public float StartFollowDelay = 1;
    public float RepearingDuration = 2;
    public float StayBrokenDuration = 3;


    private GameObject Character;
    private GnomeState CurrentState;
    private float T;
    private float T2;
    Dictionary<Transform, Vector3> InitPartPositions = new Dictionary<Transform, Vector3>();
    private float CurrentRotation;
    private float TurnSmoothVelocity;

    private void Start() {
        Color color = Random.ColorHSV(0, 1, .1f, .4f, .9f, 1);
        //transform.GetChild(0).GetComponent<Renderer>().material.SetColor("_Color", color);
        foreach (Transform item in GnomeParts.transform) {
            item.GetComponent<Renderer>().material.SetColor("_Color", color);
        }

        Character = GameObject.Find("Character");
        WalkSpeed *= Random.Range(1, 1.2f);
        StayBrokenDuration *= Random.Range(1, 1.2f);
        RepearingDuration *= Random.Range(1, 1.2f);
        TurnSmoothTime *= Random.Range(1, 1.2f);

        SetState(GnomeState.Idle);
    }

    private void Update() {

        if (transform.position.y < -10) {
            transform.position = Vector3.zero;
            GetComponent<Rigidbody>().velocity = Vector3.zero;
        }

        switch (CurrentState) {
            case GnomeState.NONE:
                break;
            case GnomeState.Idle:
                ExecuteStateIdle();
                break;
            case GnomeState.Broken:
                ExecuteStateBroken();
                break;
            case GnomeState.Follow:
                ExecuteStateFollow();
                break;
            case GnomeState.Repearing:
                ExecuteStateRepearing();
                break;
            case GnomeState.RunAway:
                ExecuteRunAway();
                break;
            default:
                break;
        }

        if (Input.GetKeyDown(KeyCode.F))
            if (CurrentState != GnomeState.Broken)
                SetState(GnomeState.Follow);

        if (Input.GetKeyDown(KeyCode.I))
            if (CurrentState != GnomeState.Broken)
                SetState(GnomeState.Idle);

    }

    private void SetState(GnomeState newState) {
        if (newState == CurrentState) return;

        // exit previous state
        switch (CurrentState) {
            case GnomeState.NONE:
                break;
            case GnomeState.Idle:
                ExitStateIdle();
                break;
            case GnomeState.Broken:
                ExitStateBroken();
                break;
            case GnomeState.Repearing:
                ExitStateRepearing();
                break;
            case GnomeState.Follow:
                ExitStateFollow();
                break;
            case GnomeState.RunAway:
                ExitStateRunAway();
                break;
            default:
                break;
        }


        // update state var
        CurrentState = newState;
        Debug.Log(newState.ToString());

        // enter new state
        switch (newState) {
            case GnomeState.NONE:
                break;
            case GnomeState.Idle:
                EnterStateIdle();
                break;
            case GnomeState.Broken:
                EnterStateBroken()
                    ;
                break;
            case GnomeState.Repearing:
                EnterStateRepearing();
                break;
            case GnomeState.Follow:
                EnterStateFollow();
                break;
            case GnomeState.RunAway:
                EnterStateRunAway();
                break;
            default:
                break;
        }
    }

    // STATE IDLE
    private void EnterStateIdle() {
        foreach (Transform item in GnomeParts.transform) {
            item.GetComponent<Rigidbody>().velocity = Vector3.zero;
        }
        //GetComponent<Rigidbody>().isKinematic = false;
        //GetComponent<Rigidbody>().velocity = Vector3.zero;
        GetComponent<Collider>().enabled = true;
        T = 0;
    }

    private void ExecuteStateIdle() {
        Vector3 lookAtDiff = Character.transform.position - transform.position;
        Vector3 lookAt = lookAtDiff.normalized;

        // rotate
        float targetRotation = Mathf.Atan2(lookAt.x, lookAt.z) * Mathf.Rad2Deg;
        CurrentRotation = Mathf.SmoothDampAngle(CurrentRotation, targetRotation, ref TurnSmoothVelocity, TurnSmoothTime);
        transform.eulerAngles = Vector3.up * CurrentRotation;

        if (IsWatching()) {
            if (lookAtDiff.magnitude < StartRunningDist) {
                SetState(GnomeState.RunAway);
            }
            else if (lookAtDiff.magnitude > StartFollowingDist * 1.5f)
                SetState(GnomeState.Follow);

            T += Time.deltaTime;
            Mocking(T);
        }
        else {
            T += Time.deltaTime;
            if (T >= StartFollowDelay || lookAtDiff.magnitude > StartFollowingDist)
                if (T >= StartFollowDelay)
                    SetState(GnomeState.Follow);
        }
    }

    private void ExitStateIdle() {
        GnomeModel.transform.localRotation = Quaternion.identity;
    }





    // STATE BROKEN

    private void EnterStateBroken() {
        GetComponent<Rigidbody>().useGravity = false;
        GetComponent<Collider>().enabled = false;
        GnomeParts.SetActive(true);
        GnomeModel.SetActive(false);
        foreach (Transform item in GnomeParts.transform) {
            item.localPosition = Center + Random.insideUnitSphere * PartsSpawnRadius;
            item.GetComponent<Rigidbody>().velocity += Random.insideUnitSphere * ExplosionVelocity;
        }
        T = 0;
    }

    private void ExecuteStateBroken() {
        if (Input.GetKeyDown(KeyCode.R)) {
            SetState(GnomeState.Repearing);
        }

        if (T < StayBrokenDuration) {
            T += Time.deltaTime;
            if (T >= StayBrokenDuration)
                SetState(GnomeState.Repearing);
        }
    }

    private void ExitStateBroken() {
    }






    // STATE REPEARING

    private void EnterStateRepearing() {
        foreach (Transform item in GnomeParts.transform) {
            if (InitPartPositions.ContainsKey(item))
                InitPartPositions[item] = item.localPosition;
            else
                InitPartPositions.Add(item, item.localPosition);
        }
        T = 0;
    }

    private void ExecuteStateRepearing() {
        if (T < RepearingDuration) {
            T += Time.deltaTime;
            float t01 = T / RepearingDuration;
            foreach (Transform item in GnomeParts.transform) {
                item.localPosition = Vector3.Lerp(InitPartPositions[item], Center, t01);
            }

            if (T >= RepearingDuration) {
                SetState(GnomeState.Idle);
            }
        }
    }

    private void ExitStateRepearing() {
        GnomeParts.SetActive(false);
        GnomeModel.SetActive(true);
        GetComponent<Rigidbody>().useGravity = true;
    }





    //STATE FOLLOW

    private void EnterStateFollow() {
        GetComponent<Collider>().enabled = true;
        T = Random.value * Mathf.PI * 2;
    }

    private void ExitStateFollow() {
        GnomeModel.transform.localRotation = Quaternion.identity;
    }

    private void ExecuteStateFollow() {
        transform.LookAt(Character.transform);
        Vector3 lookAtDiff = Character.transform.position - transform.position;
        Vector3 lookAtDirr = lookAtDiff.normalized;// * Mathf.Lerp(1, 0, lookAtDiff.magnitude / FollowRange);
        Debug.DrawRay(transform.position, lookAtDirr, Color.yellow);

        Vector3 avoid = Vector3.zero;
        foreach (Transform other in InstancesHolder.transform) {

            //for (int i = 0; i < NearbyGnomes.Count; i++) {
            //if (gameObject.name == "Gnome (1)") Debug.Log(string.Format("i:{0}, name:{1}", i, NearbyGnomes[i].name));
            if (ReferenceEquals(other.gameObject, gameObject) || !other.gameObject.active) continue;

            //Debug.Log(this + ", " + other);
            //Debug.DrawLine(other.position, transform.position, Color.magenta);

            Vector3 diff = transform.position - other.position;
            Vector3 dirr = diff.normalized * Mathf.Lerp(1, 0, Mathf.Clamp01(diff.magnitude / AvoidRange));
            avoid += dirr;
            Debug.DrawRay(transform.position, dirr, Color.red);
        }
        avoid = avoid.normalized;
        //Debug.DrawRay(transform.position, avoid, Color.red);

        Vector3 final = Vector3.Lerp(lookAtDirr, avoid, FollowRatio).normalized;
        Debug.DrawRay(transform.position, final, Color.green);


        // rotate
        float targetRotation = Mathf.Atan2(final.x, final.z) * Mathf.Rad2Deg;
        CurrentRotation = Mathf.SmoothDampAngle(CurrentRotation, targetRotation, ref TurnSmoothVelocity, TurnSmoothTime);
        transform.eulerAngles = Vector3.up * CurrentRotation;

        //float speedFactor = SpeedCurve.Evaluate(Mathf.Clamp01((lookAtDiff.magnitude - MinFollowRange) / FollowFade));
        float speedFactor = SpeedCurve.Evaluate(Mathf.InverseLerp(MinFollowRange, MinFollowRange + FollowFade, lookAtDiff.magnitude));
        //Debug.Log("speedFactor: " + Mathf.InverseLerp(MinFollowRange, MinFollowRange + FollowFade, lookAtDiff.magnitude) + ", distance: " + lookAtDiff.magnitude);

        // move
        float speed = WalkSpeed * Mathf.Lerp(0, 1, speedFactor);
        transform.position += transform.forward * speed * Time.deltaTime;

        // jumping
        float jumpHeight = JumpHeight * Mathf.Lerp(0, 1, speedFactor);
        float jumpSpeed = JumpSpeed * Mathf.Lerp(0.5f, 1, speedFactor);
        T += Time.deltaTime * jumpSpeed;
        GnomeModel.transform.localPosition = Vector3.up * Mathf.Abs(Mathf.Sin(T)) * jumpHeight;

        // detect looking at gnome
        if (IsWatching() && lookAtDiff.magnitude < StartFollowingDist)
            SetState(GnomeState.Idle);

        if (speedFactor <= 0.01f) {
            T2 += Time.deltaTime;
            Mocking(T2);
        }
        else {
            T2 = 0;
            GnomeModel.transform.localRotation = Quaternion.identity;
        }
    }





    // STATE RUN AWAY

    private void EnterStateRunAway() {
    }

    private void ExecuteRunAway() {
        GnomeModel.transform.localRotation = Quaternion.identity;

        transform.LookAt(Character.transform);
        Vector3 lookAtDiff = Character.transform.position - transform.position;
        Vector3 lookAtDirr = lookAtDiff.normalized;// * Mathf.Lerp(1, 0, lookAtDiff.magnitude / FollowRange);
        Debug.DrawRay(transform.position, lookAtDirr, Color.yellow);

        // rotate
        float targetRotation = Mathf.Atan2(-lookAtDirr.x, -lookAtDirr.z) * Mathf.Rad2Deg;
        CurrentRotation = Mathf.SmoothDampAngle(CurrentRotation, targetRotation, ref TurnSmoothVelocity, TurnSmoothTime);
        transform.eulerAngles = Vector3.up * CurrentRotation;

        //float speedFactor = SpeedCurve.Evaluate(1 - Mathf.Clamp01((lookAtDiff.magnitude) / StopRunningDist));
        float speedFactor = SpeedCurve.Evaluate(Mathf.InverseLerp(StopRunningDist, StopRunningDist - FollowFade, lookAtDiff.magnitude));
        Debug.Log("speedFactor: " + Mathf.InverseLerp(StopRunningDist, StopRunningDist - FollowFade, lookAtDiff.magnitude) + ", distance: " + lookAtDiff.magnitude);

        // move
        float speed = RunSpeed * Mathf.Lerp(0, 1, speedFactor);
        transform.position += transform.forward * speed * Time.deltaTime;

        // jump animation
        float jumpHeight = JumpHeight * speedFactor;// * Mathf.Lerp(0, 1, speedFactor);
        float jumpSpeed = JumpSpeed * speedFactor;// * Mathf.Lerp(0.5f, 1, speedFactor);
        T += Time.deltaTime * jumpSpeed;
        GnomeModel.transform.localPosition = Vector3.up * Mathf.Abs(Mathf.Sin(T)) * jumpHeight;


        //// detect looking at gnome
        //float lookingDir = Vector3.Dot(transform.forward, Character.transform.forward);
        //if (lookingDir < 0.3f)
        //    SetState(GnomeState.RunAway);
        if (lookAtDiff.magnitude > StopRunningDist * 0.999f)
            SetState(GnomeState.Idle);
    }

    private void ExitStateRunAway() {
        GnomeModel.transform.localPosition = Vector3.zero;
    }


    private void OnCollisionEnter(Collision collision) {
        if (collision.gameObject.name == "Character" && (CurrentState == GnomeState.Idle || CurrentState == GnomeState.Follow || CurrentState == GnomeState.RunAway)) {
            SetState(GnomeState.Broken);
        }
    }

    private bool IsWatching() {
        return Vector3.Dot(transform.forward, Character.transform.forward) < WatchingThreshold;
    }

    private void Mocking(float t) {
        t = Mathf.Clamp(t * 2 - 1, 0, Mathf.Infinity);
        Vector3 euler = GnomeModel.transform.localEulerAngles;
        euler.z = MockingCurve.Evaluate((t) % 1) * 10;// Mathf.Sin(T * 2) * 10;
        GnomeModel.transform.localEulerAngles = Vector3.Lerp(Vector3.zero, euler, t);
    }

    private void OnDrawGizmos() {
        Gizmos.color = Color.red;
        Gizmos.DrawSphere(transform.position, 0.05f);
    }

    //private void JumpAnimation(float t) {
    //    float jumpHeight = JumpHeight * Mathf.Lerp(0, 1, speedFactor);
    //    float jumpSpeed = JumpSpeed;// * Mathf.Lerp(0.5f, 1, speedFactor);
    //    T += Time.deltaTime * jumpSpeed;
    //    GnomeModel.transform.localPosition = Vector3.up * Mathf.Abs(Mathf.Sin(T)) * jumpHeight;
    //}
}
