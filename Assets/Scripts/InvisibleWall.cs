﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InvisibleWall : MonoBehaviour {
    [SerializeField] private float Radius = 10;
    [SerializeField] private Vector3 Offset;
    [SerializeField] private Transform Character;
    [SerializeField] private Transform CollisionPlane;
    [SerializeField] private Transform Light;
    [SerializeField] private float ScalePower = 100;
    [SerializeField] private float ScaleFactor = 50;

    // Start is called before the first frame update
    void Start() {

    }

    // Update is called once per frame
    void LateUpdate() {
        Vector3 diff = Character.position - Offset;
        Vector3 dir = diff;
        dir.y = 0;
        dir.Normalize();

        CollisionPlane.rotation = Quaternion.LookRotation(dir);
        CollisionPlane.position = Offset + dir * Radius + Vector3.up;
        float t01 = diff.sqrMagnitude / (Radius * Radius);
        CollisionPlane.localScale = Vector3.Lerp(Vector3.zero, Vector3.one * ScaleFactor, Mathf.SmoothStep(0, 1, Mathf.Pow(t01, ScalePower)));

        Light.position = CollisionPlane.position + CollisionPlane.forward;
    }

    private void OnDrawGizmos() {
        Gizmos.DrawWireSphere(transform.position + Offset, Radius);
    }
}
