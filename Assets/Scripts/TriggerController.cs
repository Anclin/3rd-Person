﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TriggerType {
    NONE,
    Tunnel,
    LevelVoid,
}

public class TriggerController : MonoBehaviour {

    [SerializeField] private TriggerType Type;
    [SerializeField] private GameController GameController;

    private void OnTriggerEnter(Collider other) {
        switch (Type) {
            case TriggerType.NONE:
                break;
            case TriggerType.Tunnel:
                GameController.OnTunnelTrigger(other);
                break;
            case TriggerType.LevelVoid:
                GameController.OnLevelVoidTrigger(other);
                break;
        }
    }
}