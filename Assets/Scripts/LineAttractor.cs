﻿using UnityEngine;

public class LineAttractor : MonoBehaviour {
    public Transform P0;
    public Transform P1;
    public LineAttractorData Data;
}
