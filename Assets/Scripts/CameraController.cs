﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {
    [Header("Refs")]
    [SerializeField] private Camera Camera;
    [SerializeField] private Transform CharTransform;
    [SerializeField] private Transform CameraTarget = null;

    [Header("Settings")]
    [SerializeField] private Vector2 CameraSpeed = new Vector2(.3f, .1f);
    [SerializeField] private float CameraDistance = 5;
    [SerializeField] private Vector2 MinMaxAngle = new Vector2(30, 60);
    [SerializeField] private float MoveDampTowards = 0.01f;
    [SerializeField] private float MoveDampAway = 1;
    [SerializeField] private float CollisionWidth = 3;
    [SerializeField] private float CameraTargetDamp = 1;
    [SerializeField] private float CameraTargetRotDamp = 1;
    [SerializeField] private float SideOffset = 0.1f;
    [SerializeField] private float MouseSensitivity = 10;

    [Header("Rails")]
    [SerializeField] private List<Bezier> Rails = new List<Bezier>();
    [SerializeField] private bool UseRails = false;
    [SerializeField] private bool RailOrTunnel = true;
    [SerializeField] private AnimationCurve RailTransitionCurve;
    [SerializeField] private float RailFadeRadius = 5;
    [SerializeField] private float RailFadeBuffer = 1;
    [SerializeField] private float RailCameraDistance = 5;
    [SerializeField] private float FOV = 30;
    [SerializeField] private float TunnelFOV = 90;
    [SerializeField] private Vector2 TunnelMinMaxAngle = new Vector2(-20, 10);
    [SerializeField] private float RailFadeTransitionDuration = 0.5f;

    [Header("DEBUG")]
    [SerializeField] private bool IgnoreMouse;
    [SerializeField] private bool RayDebug;

    private Vector3 TargetPos;
    private Vector3 FreeCamRotation = Vector3.zero;
    private float ClosestDistance;
    private BezierPoint ClosestPoint;
    private float RailFadeT = 0;
    private float CurrentCamDist;
    private Vector3 CamVel;
    private Vector3 CurrentPos;
    private Quaternion CurrentRot;
    private Vector3 CameraTargetVel;
    private Vector3 Forward;

    public float GetCameraDistance { get { return CameraDistance; } }


    private void Start() {
        CurrentPos = transform.position;
        CurrentCamDist = CameraDistance;
        foreach (var rail in Rails) {
            rail.BakeBezier();
        }
    }

    void LateUpdate() {

        Forward = Vector3.Lerp(Forward, CharTransform.forward, Time.deltaTime * CameraTargetRotDamp);

        float forwardDist = CameraTarget.localPosition.z;

        Vector3 charPos = CharTransform.position + Vector3.up * CameraTarget.localPosition.y;

        //float f = Vector3.Dot(transform.forward, Vector3.up);
        //float up01 = Mathf.InverseLerp(0.1f, 0.5f, f);
        //float down01 = Mathf.InverseLerp(-0.75f, -0.8f, f);
        //charPos += Vector3.up * up01 * 2 + Vector3.up * down01 * -3;

        // raycast boundary rays
        if (RayDebug)
            Debug.DrawRay(charPos, CameraTarget.position - charPos, Color.cyan);

        RaycastHit hit;
        int layerMask = 1 << LayerMask.NameToLayer("Environment");
        if (Physics.Raycast(charPos, CameraTarget.position - charPos, out hit, CameraDistance, layerMask)) {
            if (RayDebug) {
                Debug.DrawLine(charPos, hit.point, Color.red);
                //Debug.Log("hit: " + hit.collider.gameObject);
            }
            forwardDist = hit.distance * 0.5f;
        }

        //if (up01 > 0) {
        //    forwardDist = 0;
        //    CurrentCamDist *= 2;
        //}
        //if (down01 > 0)
        //    forwardDist *= 0.5f;

        // move to side when in front of the camera
        float dotF = Mathf.InverseLerp(0.25f, 1, Vector3.Dot(transform.forward, CharTransform.forward));
        //float dotR = Vector3.Dot(transform.forward, CharTransform.right) > 0 ? 1 : -1;
        Forward += CharTransform.right * dotF * SideOffset;


        TargetPos = Vector3.SmoothDamp(TargetPos, charPos + Forward * forwardDist, ref CameraTargetVel, CameraTargetDamp);

        // free camera position calculation
        FreeCamCollision();
    }

    private void FreeCamCollision() {
        // rotate camera
        if (!IgnoreMouse) {
            FreeCamRotation += new Vector3(-Input.GetAxis("Mouse Y") * CameraSpeed.y,
                                          Input.GetAxis("Mouse X") * CameraSpeed.x) * Time.deltaTime * 60 * MouseSensitivity;
        }
        FreeCamRotation += new Vector3(Input.GetAxis("RightStickY") * CameraSpeed.y,
                                      Input.GetAxis("RightStickX") * CameraSpeed.x) * Time.deltaTime * 60;

        // !DOES NOT SUPPORT RAILS! check out FreeCam()
        FreeCamRotation.x = Mathf.Clamp(FreeCamRotation.x, MinMaxAngle.x, MinMaxAngle.y);
        transform.eulerAngles = FreeCamRotation;

        // update camera position, after it was rotated
        transform.position = TargetPos - transform.forward * CurrentCamDist;


        // check for object between camera and character
        float shortestDist = CameraDistance;
        {
            // calculate boundary rays
            float FOV = Camera.fieldOfView;
            float aspect = Camera.aspect;
            float z = Camera.nearClipPlane;
            float x = Mathf.Tan(FOV * Mathf.Deg2Rad / CollisionWidth) * z;
            float y = x / aspect;
            Vector3[] clipPoints = {
                transform.position + transform.forward * z,
                transform.position + (transform.rotation * new Vector3(x,y,z)),
                transform.position + (transform.rotation * new Vector3(-x,y,z)),
                transform.position + (transform.rotation * new Vector3(-x,-y,z)),
                transform.position + (transform.rotation * new Vector3(x,-y,z)),
            };

            int layerMask = 1 << LayerMask.NameToLayer("Environment");

            // raycast boundary rays
            foreach (var point in clipPoints) {
                if (RayDebug)
                    Debug.DrawLine(TargetPos, point, Color.yellow);

                RaycastHit hit;
                if (Physics.Raycast(TargetPos, point - TargetPos, out hit, CameraDistance, layerMask)) {
                    if (RayDebug) {
                        Debug.DrawLine(TargetPos, hit.point, Color.red);
                        //Debug.Log("hit: " + hit.collider.gameObject);
                    }
                    if (hit.distance < shortestDist) {
                        shortestDist = hit.distance;
                    }
                }
            }
        }

        // camera position calculation
        float damp = shortestDist > CurrentCamDist ? MoveDampAway : MoveDampTowards;
        CurrentCamDist = Mathf.Lerp(CurrentCamDist, Mathf.Min(CameraDistance, shortestDist), Time.deltaTime * (1 / damp));
        transform.position = TargetPos - transform.forward * CurrentCamDist;
        transform.rotation = Quaternion.LookRotation(transform.forward);
    }

    private float GetT01PositionBased() {
        return Mathf.InverseLerp(RailFadeRadius + RailFadeBuffer, RailFadeRadius, ClosestDistance);
    }

    private float GetT01TimeSwitchBased() {
        bool closerToRail = ClosestDistance < RailFadeRadius;
        RailFadeT += Time.deltaTime * (closerToRail ? 1.0f : -1.0f);
        RailFadeT = Mathf.Clamp(RailFadeT, 0.0f, RailFadeTransitionDuration);
        float t01 = Mathf.Clamp01(RailFadeT / RailFadeTransitionDuration);
        return t01;
    }

    private Ray FreeCam() {

        // rot
        if (!IgnoreMouse) {
            FreeCamRotation += new Vector3(-Input.GetAxis("Mouse Y") * CameraSpeed.y,
                                          Input.GetAxis("Mouse X") * CameraSpeed.x) * Time.deltaTime * 60 * MouseSensitivity;
        }
        FreeCamRotation += new Vector3(Input.GetAxis("RightStickY") * CameraSpeed.y,
                                      Input.GetAxis("RightStickX") * CameraSpeed.x) * Time.deltaTime * 60;
        Vector2 currentMinMaxAngle = Vector2.Lerp(MinMaxAngle, TunnelMinMaxAngle, RailFadeT / RailFadeTransitionDuration);
        FreeCamRotation.x = Mathf.Clamp(FreeCamRotation.x, currentMinMaxAngle.x, currentMinMaxAngle.y);
        transform.eulerAngles = FreeCamRotation;

        // FOV fade
        Camera.fieldOfView = Mathf.Lerp(FOV, TunnelFOV, RailFadeT / RailFadeTransitionDuration);


        // free camera position calculation
        Vector3 pos = TargetPos - transform.forward * CameraDistance;
        Vector3 dir = transform.forward;
        return new Ray(pos, dir);
    }

    private void CalculateClosestRailDistance() {
        // get closest point
        ClosestDistance = Mathf.Infinity;
        Bezier closestRail;
        foreach (Bezier rail in Rails) {
            BezierPoint cp = rail.GetClosestPoint(TargetPos);
            float d = Vector3.Distance(cp.Position, TargetPos);
            if (d < ClosestDistance) {
                ClosestDistance = d;
                closestRail = rail;
                ClosestPoint = cp;
            }
        }
    }

    private Ray RailCam() {
        CalculateClosestRailDistance();

        // rail alignment
        Vector3 segmentDir = (ClosestPoint.SegmentP1 - ClosestPoint.SegmentP0).normalized;
        int segmentAlligned = 1;// Vector3.Dot(CharTransform.forward, railSegmentDir) > 0 ? 1 : -1;

        Debug.Log(string.Format("ClosestDistance:{0}, inverseLerp:{1}, RailFadeT:{2}", ClosestDistance, Mathf.InverseLerp(5, 2, ClosestDistance), RailFadeT));
        Vector3 pos = ClosestPoint.Position - segmentDir * RailCameraDistance * segmentAlligned;
        Vector3 dir = segmentDir * segmentAlligned;
        return new Ray(pos, dir);
    }

    private Ray TunnelCam() {
        CalculateClosestRailDistance();

        // rail alignment
        Vector3 pos = TargetPos - transform.forward * RailCameraDistance;
        Vector3 dir = transform.forward;
        return new Ray(pos, dir);
    }

    private void OnDrawGizmos() {
        if (Application.isPlaying && Rails.Count > 0) {
            Vector3 railSegmentDir = (ClosestPoint.SegmentP1 - ClosestPoint.SegmentP0).normalized;
            Gizmos.color = Color.cyan;
            Gizmos.DrawRay(TargetPos, railSegmentDir);

            // draw fade radius
            Color color = Color.magenta;
            foreach (var rail in Rails) {
                foreach (Vector3 p in new List<Vector3>(){
                rail.GetPoints[0].position,
                rail.GetPoints[rail.GetPoints.Count-1].position }
                ) {
                    Gizmos.color = color;
                    Gizmos.DrawWireSphere(p, RailFadeRadius);
                    Gizmos.color = color * .75f;
                    Gizmos.DrawWireSphere(p, RailFadeRadius + RailFadeBuffer);
                }
            }
        }
    }
}
