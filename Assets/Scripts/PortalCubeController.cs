﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PortalCubeController : MonoBehaviour {


    [SerializeField] private List<Transform> Locations;


    [SerializeField] private Transform Char;
    [SerializeField] private CameraController Camera;

    public static PortalCubeController Instance;

    private Dictionary<PortalType, int> PortalMapping = new Dictionary<PortalType, int>();

    void Awake() {
        if (Instance == null)
            Instance = this;

        PortalMapping.Add(PortalType.Yellow, 1);
        PortalMapping.Add(PortalType.Green, 1);
        PortalMapping.Add(PortalType.Red, 0);
        PortalMapping.Add(PortalType.Blue, 2);
        PortalMapping.Add(PortalType.Magenta, 3);
    }

    public void Teleport(PortalType sourcePortalType, Transform sourcePortal) {

        Transform destinationLocation = Locations[PortalMapping[sourcePortalType]];

        Debug.Log(sourcePortal.name);
        if (Vector3.Dot(Char.forward, sourcePortal.forward) > 0 && Vector3.Dot(Camera.transform.forward, sourcePortal.forward) > 0) {
            Char.position = destinationLocation.position;
        }
    }
}