﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkyParticles : MonoBehaviour {

    CharController Character;

    void Start() {
        Character = FindObjectOfType<CharController>();
    }

    // Update is called once per frame
    void Update() {
        transform.position = Character.transform.position;
    }
}
