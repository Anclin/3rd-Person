﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpotlightTrigger : MonoBehaviour {

    [Header("REF")]
    [SerializeField] private Light SpotLight;
    [SerializeField] private GameObject SpotLightCone;
    [SerializeField] private GameObject SpotLightFloor;
    [SerializeField] private GameObject Doors;
    [SerializeField] private GameObject Tunnel;
    [SerializeField] private BoxCollider BoxCollider;

    [Header("SETTINGS")]
    [SerializeField] private float Intensity1;
    [SerializeField] private float Intensity2;
    [SerializeField] private float CloseDuration = 1;
    [SerializeField] private float OpenDuration = 1;
    [SerializeField] private AnimationCurve CloseAnimationCurve;
    [SerializeField] private AnimationCurve OpenAnimationCurve;

    private bool WentThroughDoors;
    private bool OpenDoors;
    private float T = 0;


    private void Start() {
        SpotLight.intensity = Intensity1;
        SpotLightCone.SetActive(false);
        SpotLightFloor.SetActive(false);
        Tunnel.SetActive(false);
    }

    private void Update() {
        T -= Time.deltaTime;
        if (OpenDoors) {
            float t01 = Mathf.InverseLerp(OpenDuration, 0, T);
            float t = OpenAnimationCurve.Evaluate(t01);
            float angle = Mathf.Lerp(0, -100, t);
            Doors.transform.localRotation = Quaternion.Euler(0, angle, 0);
        }
        else {
            float t01 = Mathf.InverseLerp(CloseDuration, 0, T);
            float t = CloseAnimationCurve.Evaluate(t01);
            float angle = Mathf.Lerp(-100, 0, t);
            Doors.transform.localRotation = Quaternion.Euler(0, angle, 0);
        }
    }

    private void OpenCloseDoors(bool open) {
        SpotLight.intensity = open ? Intensity2 : Intensity1;
        SpotLightCone.SetActive(open);
        SpotLightFloor.SetActive(open);
        Tunnel.SetActive(open);
        OpenDoors = open;
        T = open ? OpenDuration : CloseDuration;

        if (open) {
            BoxCollider.size = new Vector3(2, 5.6f, 20);
            BoxCollider.center = new Vector3(0, 2, 9);
        }
        else {
            BoxCollider.size = new Vector3(2, 5.6f, 4);
            BoxCollider.center = new Vector3(0, 2, 1);
        }
    }

    private void OnTriggerEnter(Collider other) {
        Debug.Log("enter: ", other);
        OpenCloseDoors(true);
    }

    private void OnTriggerExit(Collider other) {
        Debug.Log("exit");
        OpenCloseDoors(false);
    }
}