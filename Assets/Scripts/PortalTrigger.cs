﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PortalType {
    NONE,
    Yellow,
    Green,
    Red,
    Blue,
    Magenta,
}

public class PortalTrigger : MonoBehaviour {

    [SerializeField] private PortalType PortalType;

    private void OnTriggerExit(Collider other) {
        PortalCubeController.Instance.Teleport(PortalType, transform);
    }
}