﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Portal : MonoBehaviour {

    [SerializeField] private Camera Camera;
    [SerializeField] private GameObject Character;
    [SerializeField] private GameObject PortalQuad;
    [SerializeField] private string SceneName = "Level1_ForestPath";

    private void OnTriggerExit(Collider other) {
        if (Vector3.Dot(PortalQuad.transform.forward, Camera.transform.forward) < -.1f &&
            Vector3.Dot(PortalQuad.transform.forward, Character.transform.forward) < -.1f) {
            SceneManager.LoadScene(SceneName);
        }
    }
}
